import threading
import subprocess
from plotter import plot_process_controller, settings_config, plot_process_monitor
from hard_drive_controller import hard_drive_controller
import configparser
import requests
import json
import time

class plotProcessDto(object):
    uniqueIdentifier = None
    phase = ""
    step = ""
    plotNumber = ""

    def __init__(self, identifier, phase, step, plotNumber):
        self.uniqueIdentifier = identifier
        self.phase = phase
        self.step = step
        self.plotNumber = plotNumber

class payload(object):
    def __init__(self):
        self.plottingStatuses = []
        self.hardDrives = []
        self.username = ""
        self.plotterKey = ""
    
    def addHardDrives(self, hardDrives):
        self.hardDrives = hardDrives

    def addPlots(self, plotController):
        self.plottingStatuses = []
        
        for plotMonitor in plotController.active_plot_monitors:
            plotProcess = plotProcessDto(plotMonitor.proc.pid, plotMonitor.phase, plotMonitor.step, plotMonitor.plot_number)
            self.plottingStatuses.append(plotProcess)

    def addAuth(self, username, plotterKey):
        self.username = username
        self.plotterKey = plotterKey

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        
class master_controller(object):
    plot_controllers = []
    plot_controller = plot_process_controller()
    hd_controller = hard_drive_controller()
    username = ''
    plotterKey = ''
    api_url = 'https://api.hotplotter.com/api/Plotter/UpdateStatus'

    def __init__(self, username, plotterKey):
        self.username = username
        self.plotterKey = plotterKey
    
    def start_plot(self, farm_config):
        runner = threading.Thread(target = self.plot_controller.start_plotting, args =(farm_config,))
        runner.daemon = True
        runner.start()

    #this isn't clean, but API should be agnostic of content here... probably would require a refactor on the settings/plotter, and i'm not in the mood to do that.
    def transformToPlotConfig(self, queuedPlot):
        config = {}
        config['drives'] = [{"destination_drive":queuedPlot['finalDrive'], "temp_drive":queuedPlot['plotDrive'], "parallel_plots": queuedPlot['parallelPlots']}]
        config['size'] = queuedPlot['size']
        config['stagger_type'] = queuedPlot['staggerType']
        config['pool_key'] = queuedPlot['poolKey']
        config['farm_key'] = queuedPlot['farmKey']
        config['ram'] = queuedPlot['ram']
        config['stagger_time'] = queuedPlot['staggerTime']
        config['threads'] = queuedPlot['threads']
        config['count'] = queuedPlot['count']
        return config
        
    def heart_beat(self):
        heartbeat_interval = 120.0
        
        postData = payload()
        postData.addHardDrives(self.hd_controller.get_hard_drives())
        postData.addPlots(self.plot_controller)
        postData.addAuth(self.username, self.plotterKey)
        try:
            x = requests.post(self.api_url, data=postData.toJSON(), headers={"Content-Type":"application/json"})
            if(x.ok):
                for plotToRun in x.json():
                    print("Plot Request Received")
                    transformedConfig = self.transformToPlotConfig(plotToRun)
                    print(str(transformedConfig))
                    run_config = settings_config(transformedConfig)
                    self.start_plot(run_config)
            else:
                if x.status_code == 400:
                    raise Exception("Please confirm your username and plotter key are correct!")
                else:
                    raise Exception("Error: " + str(x.status_code))
        except requests.ConnectionError as err:
            print("API Error. " + str(err) + ". Retrying in " + str(heartbeat_interval))
            
        heartbeat = threading.Timer(heartbeat_interval, self.heart_beat)
        heartbeat.daemon = True
        heartbeat.start()
        
        
#delete:
def generate_config():
    parser = configparser.RawConfigParser()
    parser.read('plotter_config.txt')

    controller_config = settings_config(dict(parser.items('Settings')))
    return controller_config
        
def main():
    parser = configparser.RawConfigParser()
    parser.read('plotter_config.txt')
    settings = dict(parser.items('Settings'))
    controller = master_controller(settings['username'], settings['plotterkey'])
    controller.heart_beat()
        
    
if __name__ == '__main__':
    main()
    while(True):
        time.sleep(3)
