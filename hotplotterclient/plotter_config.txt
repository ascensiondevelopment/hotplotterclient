[Settings]
farm_key = your farm key
pool_key = your pool key
ram = 4500
threads = 3
size = 32
drives = [{"destination_drive":"S:/", "temp_drive":"D:/", "parallel_plots":2}]

#time between each waves in number of staggers start - 1 hour
stagger_time = 5400

count = 8
parallel_plots = 3
validate_drives = False

stagger_type = time

username = your username
plotterKey = your plotter key