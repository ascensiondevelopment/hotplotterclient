import subprocess
import threading
import time
import configparser
import re
import shutil
from queue import Queue
import datetime
import json

class plot_process_monitor(object):
    proc = None
    start_time = None
    temp_drive = ""
    log_file_name = ""
    is_alive = True
    phase = 0
    file_name = ""
    step = ""
    plot_number = 0
    number_of_iterations = 0
    
    def __init__(self, proc, temp_drive, destination_drive, number_of_iterations):
        self.proc = proc
        self.temp_drive = temp_drive
        self.destination_drive = destination_drive
        self.file_name = self.temp_drive.replace(':', '').replace('/', '') + "_" + self.destination_drive.replace(':', '').replace('/', '') + "_" + str(self.proc.pid)+".txt"
        self.start_time = datetime.datetime.now()
        self.number_of_iterations = number_of_iterations
        
    def start_monitoring(self):
        self.log_to_file("Starting: " + self.temp_drive + " " + self.destination_drive + " with PID of: " + str(self.proc.pid))
        while self.proc.poll() is None:
            line = self.proc.stdout.readline()
            self.step = line

            if("Starting phase 1/4".lower() in line.lower()):
                self.plot_number += 1
            
            parsed_phase = self.get_phase(line)
            if parsed_phase != 0:
                self.phase = parsed_phase
                
            if not line:
                self.is_alive = False
                break

            self.log_to_file(line)

    def log_to_file(self, line):
        with open(self.file_name, 'a+') as file:
            file.write(self.temp_drive + " " + self.destination_drive + ": " + str(line) + '\n')

    def get_phase(self, content):
        lower_content = content.lower()
        if "phase 1" in lower_content:
            return 1
        if "phase 2" in lower_content:
            return 2
        if "phase 3" in lower_content:
            return 3
        if "phase 4" in lower_content:
            return 4
        if "phase" not in lower_content:
            return 0
    
class settings_config(object):
    drives = []
    size = 0
    threads = 0
    ram = 0
    pool_key = ""
    farm_key = ""
    stagger_time = 0
    stagger_type = ""
    count = 0
    
    def __init__(self, dictionary_config):
        self.drives = dictionary_config["drives"]
        self.stagger_type = dictionary_config['stagger_type']
        if dictionary_config['stagger_time'] != None:
            self.stagger_time = int(dictionary_config['stagger_time'])
            
        self.farm_key = dictionary_config['farm_key']
        self.pool_key = dictionary_config['pool_key']
        self.ram = dictionary_config['ram']
        self.threads = dictionary_config['threads']
        self.size = dictionary_config['size']
        self.count = dictionary_config['count']
    
class plot_process_controller(object):
    list_of_plotting_processes = []
    log_file_name = "log_file_"
    active_plot_monitors = []
        

    def add_monitor(self, monitor):
        self.active_plot_monitors.add(monitor)

    def remove_inactive_plot_monitors(self):
        self.active_plot_monitors = [x for x in self.active_plot_monitors if x.is_alive == True]
    
    def event_listener(self, config):
        self.remove_inactive_plot_monitors()
        for plot_config in config.drives:
            iterations_left = int(plot_config['parallel_plots']) > 0
            if(iterations_left == False):
                continue
            
            #get the most recently created active plot
            active_monitors = [x for x in self.active_plot_monitors if x.temp_drive == plot_config['temp_drive'] and x.destination_drive == plot_config['destination_drive']]
                
            active_monitors.sort(key = lambda x : x.start_time, reverse = True)
            latest_monitor = active_monitors[0]

            if config.stagger_type.lower() == 'time':
                if latest_monitor.start_time < datetime.datetime.now() - datetime.timedelta(seconds = config.stagger_time):
                    print("Starting Next Plot")
                    self.start_plot(config, plot_config, len(active_monitors) + 1)
            elif config.stagger_type.lower() == 'phase':
                if latest_monitor.phase == 2:
                    print("Starting Next Plot")
                    self.start_plot(config, plot_config, len(active_monitors) + 1)
        
        listener = threading.Timer(5.0, self.event_listener, args = (config,))
        listener.daemon = True
        listener.start()
    
    
    #start initial plots
    def start_plotting(self, config):
        for plot_config in config.drives:
            count = int(plot_config["parallel_plots"])

            if count > 0:
                self.start_plot(config,  plot_config, count)

        self.event_listener(config)
                

    def start_plot(self, config, plot_config, iteration):

        plot_proc = plot_process(config, plot_config["temp_drive"], plot_config["destination_drive"])
        proc = plot_proc.begin_plotting()

        plot_config["parallel_plots"] -= 1

        monitor = plot_process_monitor(proc, plot_config["temp_drive"], plot_config["destination_drive"], iteration)

        thread = threading.Thread(target = monitor.start_monitoring)
        thread.daemon = True
        thread.start()

        self.active_plot_monitors.append(monitor)
        
class plot_process(object):
    log_file_name = ''
    drive_to_plot = ''
    final_drive = ''
    def __init__(self, config, temp_drive, final_drive):
        self.config = config
        self.temp_drive = temp_drive
        self.final_drive = final_drive        
                
    def begin_plotting(self):
        print('#Beginning Temp Plot: ' + self.temp_drive + ' Plotting to ' + self.final_drive)
        print("Count: " + str(self.config.count) + " Ram: " + str(self.config.ram) + " Threads: " + str(self.config.threads) + " Size: " + str(self.config.size) + " TempDrive: " + str(self.temp_drive) + " Final Drive: " + str(self.final_drive) + " Farm Key: " + self.config.farm_key + " Pool Key: " + self.config.pool_key)
        proc = subprocess.Popen(["chia.exe", "plots", "create", "-n", str(self.config.count), "-b", str(self.config.ram), "-r", str(self.config.threads), "-k", str(self.config.size), "-t", self.temp_drive+"TempPlots/", "-d", self.final_drive+"Plots/", "-f", self.config.farm_key, "-p", self.config.pool_key], stdout=subprocess.PIPE, text = True)
        return proc

def main():
    monitors = []
    config = generate_config()
    controller = plot_process_controller()
    runner = threading.Thread(target = controller.start_plotting, args=(config,))
    runner.start()

    
def generate_config():
    parser = configparser.RawConfigParser()
    parser.read('plotter_config.txt')
    
        
    controller_config = settings_config(dict(parser.items('Settings')))
    #if ran manually, the file loads it in as str. need to load via json, and convert to object.

    if isinstance(controller_config.drives, str):
        controller_config.drives = json.loads(controller_config.drives)
    return controller_config
    
if __name__ == '__main__':
    main()

 
